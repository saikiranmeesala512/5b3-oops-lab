import java.util.*;
class EvenorOdd {
   static void myFun(int n){
       if(n%2==0)
       {
           System.out.println("The number "+ n + " is Even.");
       }
       else{
           System.out.println("The number "+ n + " is Odd.");
       }
    }
public static void main(String[] args){
   System.out.println("Enter the number : ");
   Scanner x = new Scanner(System.in);
   int n = x.nextInt();
   myFun(n);
   x.close();
}
